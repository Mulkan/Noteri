import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput} from 'react-native';

const CreateWork = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Open up App.js to start working in your app! from CreateWork</Text>
            <TextInput style={{backgroundColor: '#ddd'}}/>
            <TouchableOpacity>
                <Text>Create Work</Text>
            </TouchableOpacity>
            <StatusBar style={'auto'}/>
        </View>
    )
}

export default CreateWork;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
      paddingBottom: 50,
  }
});
