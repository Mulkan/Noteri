import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

const NoteScreen = ({route, navigation }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Open up App.js to start working in your app! from HomeScreen</Text>
            <Button title="Go Home" onPress={() => navigation.navigate('HomeScreen')} />
            <StatusBar style={'auto'}/>
        </View>
    )
}

export default NoteScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
      paddingBottom: 50,
  }
});
