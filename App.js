import * as React from 'react';
import HomeScreen from './Conponents/HomeScreen/HomeScreen';
import NoteScreen from './Conponents/NoteScreen/NoteScreen';
import CreateWork from './Conponents/HomeScreen/CreateWork';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeScreen">
        <Stack.Screen name="HomeScreen" component={HomeScreen}/>
        <Stack.Screen name="NoteScreen" component={NoteScreen}/>
        <Stack.Screen name="CreateWork" component={CreateWork}/>
      </Stack.Navigator>
      {/* <Tabs.Navigator>
        <Tabs.Screen name="HomeScreen" component={HomeScreen}/>
        <Tabs.Screen name="NoteScreen" component={NoteScreen}/>
        <Tabs.Screen name="CreateWork" component={CreateWork}/>
      </Tabs.Navigator> */}
      {/* <Drawer.Navigator>
        <Drawer.Screen name="HomeScreen" component={HomeScreen}/>
        <Drawer.Screen name="NoteScreen" component={NoteScreen}/>
      </Drawer.Navigator> */}
    </NavigationContainer>
  );
}
